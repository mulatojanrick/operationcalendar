Vue.config.productionTip = false;
/* eslint-disable no-new */
new Vue({
  el: "#calendar",
  data: {
    json: calendar[0],
    year: '2019',
    weekdays: moment.weekdaysShort(),
    numOfDays: 7,
    weekdayCnt: 0,
    currentDayOfYear: 0,
    // flags for displaying
    displayDayOfYear: false,
    isHoliday: false
  },
  created() {
  },
  methods: {
    /**
     * Get the date from current week
     * 
     * @param {*} currentMonth 
     * @param {*} currentDayOfWeek 
     * @param {*} currentWeek 
     */
    getDate: function(currentMonth, currentDayOfWeek, currentWeek) {
      var weekday = currentWeek.days[this.weekdayCnt];

      if (weekday == undefined) {
        this.weekdayCnt = 0;
        weekday = currentWeek.days[this.weekdayCnt];
      }

      // parse the date from json
      var numb = weekday.date.match(/\d/g);
      var milisec = parseInt(numb.join(""));

      // initialize date
      var date = moment(milisec);
      var month = date.format('M');
      var dayOfWeek = date.day();
      
      // check if current date is still for the month
      if (currentMonth == month && currentDayOfWeek == dayOfWeek) {
        // set flags for display
        this.displayDayOfYear = true;
        this.isHoliday = weekday.isholiday;

        this.currentDayOfYear = weekday.dayofyear;

        // add weekday cnt
        this.weekdayCnt++;

        // check if end of the week
        if (this.weekdayCnt % 7 == 0 || dayOfWeek == 6) {
          this.weekdayCnt = 0;
        }

        return date.format('D');
      } else {
        this.displayDayOfYear = false;
        return '';
      }
    }
  }
});
